from django.conf.urls import patterns, include, url
from songs.views import hello,song,search_request, album_title, album_detail
from songs.api import SongResource

song_resource = SongResource()
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'kiwi_snow.views.home', name='home'),
    # url(r'^kiwi_snow/', include('kiwi_snow.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    
     url(r'^hello/', hello),
     url(r'^get/(?P<song_id>\d+)/$', song),
     url(r'^search/$',search_request),
     url(r'^api/',include(song_resource.urls)),
	 url(r'^albumTitle/(\d+)/$', album_title),
	 url(r'^albumDetail/(\d+)/$', album_detail),
	  
)
