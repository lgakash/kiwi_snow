from tastypie.resources import ModelResource
from tastypie.constants import ALL
from models import Song

class SongResource(ModelResource):
	class Meta:
		queryset = Song.objects.all()
		resource_name='gaana'

