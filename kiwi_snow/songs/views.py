from django.http import HttpResponse
from django.shortcuts import render_to_response
import jsonpickle as json
from models import Song, Album
from trie import Trie
import redis
# Create your views here.
KEY='songs'

def hello(request):
	print dir()
	name = Trie.time
	html="<html><body>Hi %s how the hell are you no. %s </html></body>", name
	return HttpResponse(html)

def song(request,song_id=1):
	##return HttpResponse("<html><body>...........</body></html>")

	return render_to_response('song.html',{'song':Song.objects.get(id=song_id)})


def album_title(request, album_id):
	album = Album.objects.filter(album_id=album_id)
	album = album[0]; 
	response_data={}
	response_data['albumTitle'] = album.album_name
	return HttpResponse(json.encode(response_data,unpicklable=False), mimetype='application/json')
	
def album_detail(request, album_id):
	album = Album.objects.filter(album_id=album_id)
	albumDetail=[]
	for song in album:
		songObject={}
		songObject['song_id']=song.id
		songObject['song_name']=song.song_name
		songObject['song_link']=song.song_link
		songObject['album_name']=song.album_name
		albumDetail.append(songObject)
	return HttpResponse(json.encode(albumDetail,unpicklable=False), mimetype='application/json');

def search_request(request):
	try:
		search_text=request.GET['search_text']
		songs=Song.objects.filter(song_name__contains=search_text)
	except KeyError:
		movie_search=request.GET['movie_name']
		songs=Song.objects.filter(movie_name__contains=movie_search)
	##return render_to_response('ajax_search.html',{'songs':songs})
	songList=[]
	for song in songs:
		songObject={}
		songObject['name']=song.song_name
		songObject['link']=song.song_link
		songList.append(songObject)

	response=HttpResponse(json.encode(songList,unpicklable=False));
	response["Access-Control-Allow-Origin"] = "*"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	return response
	return HttpResponse('function_callback('+ json.encode(songList,unpicklable=False)+')',content_type="application/json")


def complete(r, prefix, count):
    results = []
    rangelen = 50
    start = r.zrank(KEY, prefix)
    if not start:
        return []
    while len(results) != count:
        range = r.zrange(KEY, start, start + rangelen - 1)
        start += rangelen
        if not range or len(range) == 0:
            break
        for entry in range:
            minlen = min((len(entry), len(prefix)))
            if entry[0:minlen] != prefix[0:minlen]:
                count = len(results)
                break
            if entry[-1] == '*' and len(results) != count:
                results.append(entry[0:-1])
    return results

def auto_redis(request):
	search_request=request.GET['search_text']
	r = redis.StrictRedis(host='localhost', port=6379, db=0)
	songs = complete(r,search_request,50)
	songList=[]
	for song in songs:
			songObject={}
			songObject['name']=song
			songObject['link']=r.hget('songtolink',song)
			songList.append(songObject)

	response=HttpResponse(json.encode(songList,unpicklable=False));
	response["Access-Control-Allow-Origin"] = "*"
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
	response["Access-Control-Max-Age"] = "1000"
	response["Access-Control-Allow-Headers"] = "*"
	return response
	return HttpResponse('function_callback('+ json.encode(songList,unpicklable=False)+')',content_type="application/json")
